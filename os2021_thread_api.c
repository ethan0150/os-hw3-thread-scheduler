#include "os2021_thread_api.h"

struct itimerval Signaltimer;
ucontext_t dispatch_context;
ucontext_t finish_ctx;

trNode *readyQ = NULL,
        *waitQ = NULL,
         *termQ = NULL,
          *running = NULL;

int nextTID = 0;
long timeElapsed = 0; //ms

int OS2021_ThreadCreate(char *job_name, char *p_function, char *priority, int cancel_mode)
{
    trNode *newTr = mk_trNode(job_name, nextTID++, priority[0], cancel_mode);

    if(!strcmp(p_function, "Function1"))
        CreateContext(&(newTr->ctx), &finish_ctx, &Function1);
    else if(!strcmp(p_function, "Function2"))
        CreateContext(&(newTr->ctx), &finish_ctx, &Function2);
    else if(!strcmp(p_function, "Function3"))
        CreateContext(&(newTr->ctx), &finish_ctx, &Function3);
    else if(!strcmp(p_function, "Function4"))
        CreateContext(&(newTr->ctx), &finish_ctx, &Function4);
    else if(!strcmp(p_function, "Function5"))
        CreateContext(&(newTr->ctx), &finish_ctx, &Function5);
    else if(!strcmp(p_function, "ResourceReclaim"))
        CreateContext(&(newTr->ctx), NULL, &ResourceReclaim);
    else
    {
        free(newTr);
        return -1;
    }
    push(&readyQ, newTr);
    return newTr->tid;
}

void OS2021_ThreadCancel(char *job_name)
{
    nodeLink *searchRes = NULL;
    if(!strcmp("reclaimer", job_name))
        return;

    if(!strcmp(running->trName, job_name))
    {
        searchRes = &(nodeLink)
        {
            .cur = running,
            .last = NULL
        };
    }
    else if(searchRes = search(readyQ, &cmp_trName, job_name)) {}
    else if(searchRes = search(waitQ, &cmp_trName, job_name)) {}
    else
        printf("job \'%s\' not found.\n", job_name);

    if(searchRes->cur == running)
    {
        if(running->canMode)
        {
            running->canFlag = 1;
            printf("thread \'%s\' is tryna cancel itself.\n", running->trName);
        }
        else
        {
            push(&termQ, running);
            printf("thread \'%s\' has cancelled itself.\n", running->trName);
            running = NULL;
            setcontext(&dispatch_context);
        }
        return;
    }

    if(searchRes->cur->canMode)
    {
        searchRes->cur->canFlag = 1;
        printf("%s is tryna cancel thread %s\n", running->trName, searchRes->cur->trName);
    }
    else
    {
        if(searchRes->last)
            searchRes->last->next = searchRes->cur->next;
        else
        {
            if(searchRes->cur == readyQ)
                readyQ = readyQ->next;
            if(searchRes->cur == waitQ)
                waitQ = waitQ->next;
        }
        searchRes->cur->next = NULL;
        push(&termQ, searchRes->cur);
        printf("%s has cancelled %s.\n", running->trName, searchRes->cur->trName);
    }
    return;
}

void OS2021_ThreadWaitEvent(int event_id)
{
    printf("%s wants to wait for event %d\n", running->trName, event_id);
    running->waitEvent = event_id;
    adjPriority(running);
    push(&waitQ, running);
    swapcontext(&(running->ctx), &dispatch_context);
    return;
}

void OS2021_ThreadSetEvent(int event_id) //emit event_id
{
    nodeLink *res;
    if(res = search(waitQ, &cmp_waitEvent, &event_id))
    {
        if(res->last)
            res->last->next = res->cur->next;
        else
            waitQ = waitQ->next;
        res->cur->next = NULL;
        push(&readyQ, res->cur);
        printf("thread \'%s\' received event %d from %s.\n",
               res->cur->trName,
               event_id,
               running->trName
              );
    }
    else
    {
        printf("no thread waiting for event %d.\n", event_id);
    }
}

void OS2021_ThreadWaitTime(int msec)
{
    adjPriority(running);
    running->waitDuration = 10 * msec;
    running->next = NULL;
    push(&waitQ, running);
    swapcontext(&(running->ctx), &dispatch_context);
    return;
}

void OS2021_DeallocateThreadResource()
{
    trNode *cur;
    while(cur = pop(&termQ))
    {
        printf("memory of %s has been freed\n", cur->trName);
        free(cur->trName);
        free(cur->ctx.uc_stack.ss_sp);
        free(cur);
    }
    return;
}

void OS2021_TestCancel()
{
    if(running->canFlag)
    {
        push(&termQ, running);
        running = NULL;
        setcontext(&dispatch_context);
    }
    return;
}

void CreateContext(ucontext_t *context, ucontext_t *next_context, void *func)
{
    getcontext(context);
    context->uc_stack.ss_sp = malloc(STACK_SIZE);
    context->uc_stack.ss_size = STACK_SIZE;
    context->uc_stack.ss_flags = 0;
    context->uc_link = next_context;
    makecontext(context,(void (*)(void))func,0);
}

void ResetTimer()
{
    Signaltimer.it_value.tv_sec = 0;
    Signaltimer.it_value.tv_usec = 10000;
    if(setitimer(ITIMER_REAL, &Signaltimer, NULL) < 0)
    {
        printf("ERROR SETTING TIME SIGALRM!\n");
    }
}

void AlarmHandler()
{
    timeElapsed += 10;

    traverse(readyQ, update_readyTime, NULL);
    traverse(waitQ, update_waitTime, NULL);

    if(timeElapsed >= pri_tq[running->c_priority])
    {
        if(running->c_priority > 0)
        {
            running->c_priority--;
            printf("The priority of thread %s is changed from %c to %c\n",
                   running->trName,
                   pri_tbl[running->c_priority+1],
                   pri_tbl[running->c_priority]
                  );
        }
        push(&readyQ, running);
        swapcontext(&(running->ctx), &dispatch_context);
    }

    ResetTimer();
    return;
}

void TSTPHandler()
{
    printf("\n");
    printf("**************************************************************************************************\n");
    printf("*\tTID\tName\t\tState\t\tB_Priority\tC_Priority\tQ_Time\tW_time\t *\n");
    printf("*\t%d\t%-10s\tRUNNING\t\t%c\t\t%c\t\t%ld\t%ld\t *\n",
           running->tid,
           running->trName,
           pri_tbl[running->b_priority],
           pri_tbl[running->c_priority],
           running->readyTime,
           running->waitTime
          );
    traverse(readyQ, &print_node, "READY");
    traverse(waitQ, &print_node, "WAITING");
    printf("**************************************************************************************************\n");
    return;
}

void trFinish()
{
    push(&termQ, running);
    running = NULL;
    setcontext(&dispatch_context);
    return;
}

void Dispatcher()
{
    running = pop(&readyQ);
    running->next = NULL;
    timeElapsed = 0;
    ResetTimer();
    setcontext(&(running->ctx));
    return;
}

void StartSchedulingSimulation()
{
    /*Set Timer*/
    Signaltimer.it_interval.tv_usec = 0;
    Signaltimer.it_interval.tv_sec = 0;
    signal(SIGALRM, AlarmHandler);
    signal(SIGTSTP, TSTPHandler);
    /*Create Context*/
    CreateContext(&dispatch_context, NULL, &Dispatcher);
    CreateContext(&finish_ctx, &dispatch_context, &trFinish);
    OS2021_ThreadCreate("reclaimer", "ResourceReclaim", "L", 1);
    readJSON();
    setcontext(&dispatch_context);
}

void readJSON()
{
    JSON *json = json_object_from_file("init_threads.json"), *tr, *tmp;
    json_object_object_get_ex(json, "Threads", &json);
    size_t trNum = json_object_array_length(json);
    char *trName, *funcName;
    char *priority;
    int canMode, status;

    for(size_t i=0; i < trNum; i++)
    {
        tr = json_object_array_get_idx(json, i);

        json_object_object_get_ex(tr, "name", &tmp);
        trName = json_object_get_string(tmp);

        json_object_object_get_ex(tr, "entry function", &tmp);
        funcName = json_object_get_string(tmp);

        json_object_object_get_ex(tr, "priority", &tmp);
        priority = json_object_get_string(tmp);

        json_object_object_get_ex(tr, "canMode", &tmp);
        canMode = json_object_get_int(tmp);

        status = OS2021_ThreadCreate(trName, funcName, priority, canMode);
    }
    return;
}