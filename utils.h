#include <stdlib.h>
#include <ucontext.h>
#include <string.h>

extern long timeElapsed;
extern char pri_tbl[3];
extern int pri_tq[3];

typedef struct trnode
{
    char *trName;
    int tid;
    int b_priority;
    int c_priority;
    int canMode;
    int canFlag; // only set on threads w/ canMode == 1
    int waitEvent; // -1: w8 for waitDuration
    long readyTime;
    long waitTime;
    long waitDuration;
    long contWaitTime;

    ucontext_t ctx;
    struct trnode *next;
} trNode;

extern trNode *readyQ, *waitQ;

typedef struct nodeLink
{
    trNode *cur;
    trNode *last;
} nodeLink;

trNode* mk_trNode(const char *, int, char, int);
void push(trNode **, trNode *);
trNode* pop(trNode **);
void adjPriority(trNode *);
trNode* removeByName(trNode **, char *);
void traverse(trNode *, void (*)(nodeLink *, void *), void *);
nodeLink* search(trNode *, int (*)(void *, trNode *), void *);
int cmp_waitEvent(void *, trNode *);
int cmp_trName(void *, trNode *);
void print_node(nodeLink *, void *);
void update_readyTime(nodeLink *, void *);
void update_waitTime(nodeLink *, void *);