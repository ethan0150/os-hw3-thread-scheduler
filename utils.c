#include "utils.h"
#include <stdio.h>
char pri_tbl[3] = {'L', 'M', 'H'};
int pri_tq[3] = {300, 200, 100};

trNode* mk_trNode(const char *trName, int tid, char priority, int canMode)
{
    trNode *ret=malloc(sizeof(trNode));
    *ret = (trNode)
    {
        .tid = tid,
        .waitEvent = -1,
        .canMode = canMode
    };
    switch(priority)
    {
    case 'H':
        ret->b_priority = 2;
        break;
    case 'M':
        ret->b_priority = 1;
        break;
    case 'L':
        ret->b_priority = 0;
        break;
    }
    ret->c_priority = ret->b_priority;
    ret->trName = malloc(strlen(trName)+1);
    strcpy(ret->trName, trName);
    return ret;
}

void adjPriority(trNode *node)
{
    if(timeElapsed < pri_tq[node->c_priority])
    {
        if(node->c_priority < 2)
        {
            node->c_priority++;
            printf("The priority of thread %s is changed from %c to %c\n",
                   node->trName, pri_tbl[node->c_priority], pri_tbl[node->c_priority-1]);
        }
    }
    else if(node->c_priority > 0)
    {
        node->c_priority--;
        printf("The priority of thread %s is changed from %c to %c\n",
               node->trName, pri_tbl[node->c_priority], pri_tbl[node->c_priority+1]);
    }
    return;
}

void push(trNode **head, trNode *newNode)
{
    trNode *cur = *head,
            *last = NULL;
    if(cur)
    {
        while(cur)
        {
            if(cur->c_priority >= newNode->c_priority)
            {
                last = cur;
                cur = cur->next;
            }
            else break;
        }
    }
    if(cur)
    {
        if(last)
            last->next = newNode;
        else
            *head = newNode;
        newNode->next = cur;
    }
    else
    {
        if(last) last->next = newNode;
        else *head = newNode;
    }
    return;
}

trNode* removeByName(trNode **head, char *trName)  //returns pointer to removed node
{
    trNode *cur = *head, *last = NULL;
    while(cur)
    {
        if(!strcmp(trName, cur->trName))
        {
            if(last)
                last->next = cur->next;
            else
                *head = cur->next;
            cur->next = NULL;
            return cur;
        }
        last = cur;
        cur = cur->next;
    }
    return NULL;
}

trNode* pop(trNode **head)
{
    if(!(*head)) return NULL;
    else
    {
        trNode *ret = *head;
        *head = (*head)->next;
        ret->next = NULL;
        return ret;
    }
}

void traverse(trNode *head, void (*op)(nodeLink *, void *), void *oparg)
{
    nodeLink *curLink = &(nodeLink)
    {
        .cur = head,
        .last = NULL
    };
    while(curLink->cur)
    {
        op(curLink, oparg);
        curLink->last = curLink->cur;
        curLink->cur = curLink->cur->next;
    }
    return;
}

nodeLink* search(trNode *head, int (*cmp)(void *, trNode *), void *cmparg)
{
    trNode *cur = head, *last = NULL;
    while(cur)
    {
        if(cmp(cmparg, cur))
        {
            return &(nodeLink)
            {
                .cur = cur,
                .last = last
            };
        }
        last = cur;
        cur = cur->next;
    }
    return NULL;
}

int cmp_trName(void *name, trNode *node)
{
    if(!strcmp((const char *)name, node->trName))
        return 1;
    else
        return 0;
}

int cmp_waitEvent(void *event, trNode *node)
{
    if(node->waitEvent == *(int *)event)
        return 1;
    else
        return 0;
}

void update_readyTime(nodeLink *nl, void *emptyField)
{
    nl->cur->readyTime += 10;
    return;
}

void update_waitTime(nodeLink *nl, void *emptyField)
{
    nl->cur->waitTime += 10;
    if(nl->cur->waitEvent == -1)
    {
        nl->cur->contWaitTime += 10;
        if(nl->cur->contWaitTime >= nl->cur->waitDuration)
        {
            nl->cur->waitDuration = nl->cur->contWaitTime = 0;
            if(!nl->last)//i.e. cur == waitQ
                waitQ = nl->cur->next;
            else
                nl->last->next = nl->cur->next;
            nl->cur->next = NULL;
            push(&readyQ, nl->cur);
        }
    }
    return;
}
void print_node(nodeLink *nl, void *state)
{
    printf("*\t%d\t%-10s\t%s\t\t%c\t\t%c\t\t%ld\t%ld\t *\n",
           nl->cur->tid,
           nl->cur->trName,
           (const char *)state,
           pri_tbl[nl->cur->b_priority],
           pri_tbl[nl->cur->c_priority],
           nl->cur->readyTime,
           nl->cur->waitTime
          );
    return;
}